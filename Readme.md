#Обрабатываемые запросы

##1. Получение полной информации о пользователе
* Формат запроса
    `/users/`
    
* Формат ответа
```    
{
	"Second_name": "Андреев",
	"First_name": "Николай",
	"Email": "andreev.n.kons@gmail.com",
	"Phone_number": "+79652189669",
	"Languages": [
		"Russian",
		"English"
	],
	"Fl_guide": 0,
	"Interests": [
		"music",
		"movies"
	],
	"Sex": "male",
	"Date_of_birth": "1993-09-29T00:00:00Z",
	"Currency": "RUB",
	"City": "Moscow",
	"About": "bla",
	"Middle_name": "Константинович",
	"Photo": "photo"
}
```