FROM    nanguide/alpinego
COPY    *.go                $GOPATH/src/userspkg/
COPY    ./usersmain/*.go    $GOPATH/src/users/
RUN     go install userspkg
RUN     go install users
CMD     ["/go/bin/users"]
