package userspkg

import (
	"time"
)

type UserFullInfo struct {
	Second_name   string    `json: "Second_name"`
	First_name    string    `json: "First_name"`
	Email         string    `json: "Email"`
	Phone_number  string    `json: "Phone_number"`
	Languages     []string  `json: "Languages"`
	Fl_guide      int8      `json: "Fl_guide"`
	Interests     []string  `json: "Interests"`
	Sex           string    `json: "Sex"`
	Date_of_birth time.Time `json: "Date_of_birth"`
	Currency      string    `json: "Currency"`
	City          string    `json: "City"`
	About         string    `json: "About"`
	Middle_name   string    `json: "Middle_name"`
	Photo         string    `json: "Photo"`
}
