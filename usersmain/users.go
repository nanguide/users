package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"time"
	"userspkg"
)

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", mainHandler).Methods("GET")
	http.ListenAndServe(":8080", router)
}

func mainHandler(w http.ResponseWriter, r *http.Request) {
	user := userspkg.UserFullInfo{
		Second_name:   "Андреев",
		First_name:    "Николай",
		Email:         "andreev.n.kons@gmail.com",
		Phone_number:  "+79652189669",
		Languages:     []string{"Russian", "English"},
		Fl_guide:      0,
		Interests:     []string{"music", "movies"},
		Sex:           "male",
		Date_of_birth: time.Date(1993, time.September, 29, 0, 0, 0, 0, time.UTC),
		Currency:      "RUB",
		City:          "Moscow",
		About:         "bla",
		Middle_name:   "Константинович",
		Photo:         "photo"}
	json.NewEncoder(w).Encode(user)
}
